"""restonode URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from core import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    url(r'^django-rq/', include('django_rq.urls')),

    # rutas de cruds
    url(r'^restaurant/$', views.RestaurantList.as_view()),
    url(r'^restaurant/(?P<pk>[0-9]+)/$', views.RestaurantDetail.as_view()),
    url(r'^meal/$', views.MealList.as_view()),
    url(r'^meal/(?P<pk>[0-9]+)/$', views.MealDetail.as_view()),
    url(r'^customer/$', views.CustomerList.as_view()),
    url(r'^customer/(?P<pk>[0-9]+)/$', views.CustomerDetail.as_view()),

    # servicios
    url(r'^rate/$', views.rate),
    url(r'^place_order/$', views.place_order),
    url(r'^list_restaurants/$', views.list_restaurants),
]
