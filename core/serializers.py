from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator
from rest_framework.serializers import PrimaryKeyRelatedField,ReadOnlyField

from core.models import Restaurant, Meal,Customer,Rate,Order,OrderItem

from core.services import MapsService

class RestaurantSerializer(serializers.ModelSerializer):
    class Meta:
        model = Restaurant
        fields = ('id', 'name', 'latitude', 'longitude', 'address', 'email', 'username')

class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = ('id', 'name', 'cellPhone', 'username')

class MealSerializer(serializers.ModelSerializer):
    class Meta:
        model = Meal
        fields = ('id', 'name', 'price')

class RestaurantRatedListSerializer(serializers.ModelSerializer):
    average_rate = serializers.IntegerField()
    class Meta:
        model = Restaurant
        fields = ('id', 'name', 'address', 'latitude', 'longitude', 'average_rate')

class RateSerializer(serializers.ModelSerializer):
    restaurant = PrimaryKeyRelatedField(queryset=Restaurant.objects.all())
    customer = PrimaryKeyRelatedField(queryset=Customer.objects.all())
    value = serializers.IntegerField(required=True)
    class Meta:
        model = Rate
        fields = ('restaurant', 'value', 'customer')
        validators = [
            UniqueTogetherValidator(
                queryset=Rate.objects.all(),
                fields=('restaurant', 'customer')
            )
        ]

class OrderItemSerializer(serializers.ModelSerializer):
    meal = PrimaryKeyRelatedField(queryset=Meal.objects.all())
    class Meta:
        model = OrderItem
        fields = ('qty', 'meal')

class PlaceOrderSerializer(serializers.ModelSerializer):
    restaurant = PrimaryKeyRelatedField(queryset=Restaurant.objects.all())
    customer = PrimaryKeyRelatedField(queryset=Customer.objects.all())
    items  = OrderItemSerializer(many=True)
    order_uid = ReadOnlyField()
    order_status = ReadOnlyField()
    class Meta:
        model = Order
        fields = ('restaurant', 'latitude', 'longitude', 'address', 'customer', 'items', 'order_status', 'order_uid', 'eta')
    def create(self, validated_data):
        items = validated_data.pop('items')
        order = Order.objects.create(**validated_data)
        resto = order.restaurant

        order.eta = MapsService.eta(order.latitude,order.longitude,resto.latitude,resto.longitude, 'driving')

        order.save()
        for meal_data in items:
            OrderItem.objects.create(order=order, **meal_data)
        return order
