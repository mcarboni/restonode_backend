# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import IntegrityError
from django.shortcuts import render
from core.models import Restaurant,Customer,Meal
from core.serializers import CustomerSerializer,RestaurantSerializer,MealSerializer,RestaurantRatedListSerializer,RateSerializer,PlaceOrderSerializer
from core.tasks import restonode_send_sms,restonode_send_order

from rest_framework import generics, status, permissions
from rest_framework.decorators import api_view,permission_classes
from rest_framework.response import Response
from django.db.models import Avg
from django.db.models import IntegerField,Func
from django.core.exceptions import ObjectDoesNotExist

class Round(Func):
    function = 'ROUND'
    template='%(function)s(%(expressions)s, 0)'

class RestaurantList(generics.ListCreateAPIView):
    queryset = Restaurant.objects.all()
    serializer_class = RestaurantSerializer
    permission_classes = (permissions.IsAuthenticated,)

class RestaurantDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Restaurant.objects.all()
    serializer_class = RestaurantSerializer
    permission_classes = (permissions.IsAuthenticated,)

class MealList(generics.ListCreateAPIView):
    queryset = Meal.objects.all()
    serializer_class = MealSerializer
    permission_classes = (permissions.IsAuthenticated,)

class MealDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Meal.objects.all()
    serializer_class = MealSerializer
    permission_classes = (permissions.IsAuthenticated,)

class CustomerList(generics.ListCreateAPIView):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
    permission_classes = (permissions.IsAuthenticated,)

class CustomerDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
    permission_classes = (permissions.IsAuthenticated,)

import logging

logger = logging.getLogger(__name__)

@api_view(['POST'])
@permission_classes([permissions.IsAuthenticated,])
def rate(request):
  try:
    rateData = request.data
    customer = Customer.objects.filter(username=request.user.id).first()
    if (customer is not None):
      rateData['customer'] = customer.id
    serializer = RateSerializer(data=rateData)
    serializer.is_valid(raise_exception=True)
    serializer.save()

    return Response(serializer.data, status=status.HTTP_200_OK)
  except Exception as e:
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
@permission_classes([permissions.IsAuthenticated,])
def place_order(request):
  try:
    orderData = request.data
    customer = Customer.objects.filter(username=request.user.id).first()
    if (customer is not None):
      orderData['customer'] = customer.id
    serializer = PlaceOrderSerializer(data=orderData)
    serializer.is_valid(raise_exception=True)
    serializer.save()
    la_order = { "order_uid":serializer.data['order_uid'] }

    if ('eta' in serializer.data):
        la_order['eta'] = serializer.data['eta']

    restonode_send_order.delay(serializer.data['restaurant'], la_order)
    restonode_send_sms.delay(serializer.data['customer'], la_order)

    return Response(la_order, status=status.HTTP_200_OK)
  except Exception as e:
    print e
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def list_restaurants(request):
    res = Restaurant.objects.annotate(average_rate = Round(Avg('rate__value')))
    average_rate = request.GET.get('average_rate')
    if average_rate is not None and average_rate.isdigit():
        res = res.filter(average_rate = average_rate)
    res = res.order_by('-average_rate')
    ser = RestaurantRatedListSerializer(res, many = True)
    return Response(ser.data)
