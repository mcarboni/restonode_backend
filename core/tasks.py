import string

from django_rq import job
from core.models import Restaurant,Customer,Order

@job
def restonode_send_order(restaurant_id, order):
  print(restaurant_id)
  restaurant = Restaurant.objects.get(id=restaurant_id)
  print "email to %s: Order %s confirmed. ETA %s"%(restaurant.email, order['order_uid'], order['eta'])

@job
def restonode_send_sms(customer_id, order):
  print(customer_id)
  customer = Customer.objects.get(id=customer_id)
  print "SMS to %s: Order %s confirmed. ETA %s"%(customer.cellPhone, order['order_uid'], order['eta'])
