# -*- coding: utf-8 -*-
# Generated by Django 1.11.15 on 2018-08-12 23:26
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='address',
            field=models.CharField(default='', max_length=255),
            preserve_default=False,
        ),
    ]
