# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
import uuid

class Restaurant(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=100, blank=False, default='')
    address = models.CharField(max_length=255, blank=False, default='')
    longitude = models.DecimalField(max_digits=18, blank=False, decimal_places=15)
    latitude = models.DecimalField(max_digits=18, blank=False, decimal_places=15)
    email = models.CharField(max_length=255, blank=False, default='')
    username = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    class Meta:
        ordering = ('name',)

class Customer(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=100, blank=False)
    cellPhone = models.CharField(max_length=100, blank=False)
    username = models.ForeignKey('auth.User', on_delete=models.CASCADE)

class Rate(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE, blank=False, null=False)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, blank=False, null=False)
    value = models.IntegerField(validators=[MinValueValidator(1),
                                       MaxValueValidator(5)])

class Meal(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=100, blank=False)
    price = models.DecimalField(max_digits=10, blank=False, decimal_places=2)
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE)

class Order(models.Model):
    ORDER_STATUS = (('P','PENDING'),('C','CONFIRMED'))
    created = models.DateTimeField(auto_now_add=True)
    eta = models.DateTimeField(blank=True,null=True)
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    address = models.CharField(max_length=255, blank=False)
    status = models.CharField(max_length=255, choices=ORDER_STATUS, default = 'P')
    longitude = models.DecimalField(max_digits=18, blank=False, decimal_places=15)
    latitude = models.DecimalField(max_digits=18, blank=False, decimal_places=15)
    order_uid = models.UUIDField(default=uuid.uuid4, editable=False)

class OrderItem(models.Model):
    qty = models.DecimalField(max_digits=10, blank=False, decimal_places=2, validators=[MinValueValidator(1)])
    meal = models.ForeignKey(Meal, on_delete=models.CASCADE)
    order = models.ForeignKey(Order, related_name='items', on_delete=models.CASCADE)
