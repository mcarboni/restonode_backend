# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import googlemaps
from django.utils import timezone
from datetime import datetime,timedelta
from restonode import settings

class mapsService:
  gmaps = googlemaps.Client(key=settings.GOOGLE_API_KEY)

  def eta(self, lat_1, lng_1, lat_2, lng_2, the_mode):
    now = datetime.now(tz=timezone.utc)
    desde = "%f,%f"%(lat_1,lng_1)
    hasta = "%f,%f"%(lat_2,lng_2)
    #print(desde)
    #print(hasta)
    rutas = self.gmaps.directions(desde,hasta,mode=the_mode,departure_time=now)
    if len(rutas) > 0 and len(rutas[0]['legs']) > 0:
      duration = rutas[0]['legs'][0]['duration']
      el_eta = now + timedelta(seconds=duration['value'])
      return el_eta
    return None

MapsService = mapsService()
