 FROM python:2.7-alpine

 RUN apk update && \
     apk add  build-base python-dev py-pip jpeg-dev zlib-dev && \
     apk add postgresql-client && \
     apk add postgresql-dev
 
ENV LIBRARY_PATH=/lib:/usr/lib
 ENV PYTHONUNBUFFERED 1
 RUN mkdir /code
 WORKDIR /code
 ADD requirements.txt /code/
 RUN pip install -r requirements.txt
 ADD . /code/
 ADD wait-for-postgres.sh /code/
