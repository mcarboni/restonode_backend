RESTONODE Backend

```bash
$ docker-compose up -d

$ docker-compose exec backend python manage.py makemigrations

$ docker-compose exec backend python manage.py migrate

# check the queue
$ docker-compose exec backend python manage.py  rqworker default
```

